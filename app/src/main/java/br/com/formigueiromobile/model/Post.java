package br.com.formigueiromobile.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.formigueiromobile.model.collection.HashtagList;
import br.com.formigueiromobile.model.collection.ImageList;
import br.com.formigueiromobile.model.collection.UserList;

/**
 * Created by DANIEL on 02/09/2015.
 */

public class Post {
    public long id;
    public User author;
    public String text;
    public ImageList photos;
    public Location location;
    public Date createdAt;
    public long replyTo;
    public UploadableFile photo;
    public HashtagList hashtags;
    public UserList mentions;

    public Post() {
        photos = new ImageList();
        hashtags = new HashtagList();
    }
}
