package br.com.formigueiromobile.model;

import java.util.Date;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class Session {
    public String id;
    public Date createdAt;
    public User user;
}
