package br.com.formigueiromobile.model;

/**
 * Created by DANIEL on 25/09/2015.
 */
public class Todo {
    public Integer userId;
    public Integer id;
    public String title;
    public boolean completed;
}
