package br.com.formigueiromobile.model;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class Credentials {
    public String username;
    public String password;

    public Credentials() {}

    public Credentials(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
