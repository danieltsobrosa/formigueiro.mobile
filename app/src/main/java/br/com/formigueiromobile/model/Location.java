package br.com.formigueiromobile.model;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class Location {
    public double latitude;
    public double longitude;

    public Location() {}

    public Location(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

}
