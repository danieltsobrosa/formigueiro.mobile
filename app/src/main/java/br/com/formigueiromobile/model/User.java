package br.com.formigueiromobile.model;

import java.util.Date;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class User {
    public int id;
    public String username;
    public String password;
    public String email;
    public Image photo;
    public Image cover;
    public Date createdAt;
    public int followersCount;
    public int followingCount;
}
