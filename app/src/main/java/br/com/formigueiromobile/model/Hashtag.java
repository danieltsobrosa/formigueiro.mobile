package br.com.formigueiromobile.model;

import java.util.Date;

/**
 * Created by DANIEL on 22/10/2015.
 */
public class Hashtag {
    public String id;
    public Date createdAt;
    public int postsCount;
    public String postsUrl;
    public String url;
}
