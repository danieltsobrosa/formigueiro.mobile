package br.com.formigueiromobile.model;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class Image {
    public String url;

    public Image() {}

    public Image(String url) {
        this.url = url;
    }
}
