package br.com.formigueiromobile.model;

/**
 * Created by DANIEL on 09/11/2015.
 */
public class UploadableFile {
    public String type;
    public String content;

    public UploadableFile() {}

    public UploadableFile(String type, String content) {
        this.type = type;
        this.content = content;
    }
}
