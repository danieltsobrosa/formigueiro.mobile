package br.com.formigueiromobile.views.viewAdapters.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.views.utils.ImageUtils;

/**
 * Created by DANIEL on 04/10/2015.
 */
public abstract class ImageDetailAdapter<I> extends ArrayAdapter<I> {

    public ImageDetailAdapter(Context context, List<I> items) {
        super(context, R.layout.image_detail_item, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.image_detail_item, null);
        }

        I item = getItem(position);

        if (item != null) {
            ImageView image = (ImageView) v.findViewById(R.id.image);
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView detail = (TextView) v.findViewById(R.id.detail);
            ImageView hasImage = (ImageView) v.findViewById(R.id.hasImage);
            ImageView hasGeolocation = (ImageView) v.findViewById(R.id.hasGeolocation);

            Object src = getImage(item);

            try {
                if (src instanceof Drawable) {
                    image.setImageDrawable((Drawable) src);
                } else if (src instanceof Integer) {
                    image.setImageDrawable(getContext().getResources().getDrawable((Integer) src));
                } else if (src instanceof String) {
                    ImageUtils.displayImage(getContext(), (String) src, image);
                }
            } catch (Exception e) {
                image.setImageDrawable(null);
            }

            title.setText(getTitle(item));
            detail.setText(getDetail(item));

            hasImage.setVisibility(hasImage(item) ? View.VISIBLE : View.INVISIBLE);
            hasGeolocation.setVisibility(hasGeolocation(item) ? View.VISIBLE : View.INVISIBLE);
        }

        return v;
    }

    protected abstract Object getImage(I item);
    protected abstract String getTitle(I item);
    protected abstract String getDetail(I item);

    protected boolean hasImage(I item) {
        return false;
    }

    protected boolean hasGeolocation(I item) {
        return false;
    }
}
