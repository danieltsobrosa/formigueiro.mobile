package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.ListUsersService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroListActivity;
import br.com.formigueiromobile.views.viewAdapters.UserAdapter;
import retrofit.Response;

public class UserListActivity extends FormigueiroListActivity<User> implements ServiceClient<UserList> {
    private EditText searchTerm;
    private Button doSearch;

    public UserListActivity() {
        super(R.id.list);
    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, UserListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_list_activity);

        searchTerm = (EditText) findViewById(R.id.searchTerm);
        doSearch = (Button) findViewById(R.id.doSearch);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.users, menu);
        return true;
    }

    public void doSearchUser(View view) {
        showLoading(R.string.loading, R.string.wait);

        String term = searchTerm.getText().toString();
        Service service = new ListUsersService(term);
        executeService(service);
    }

    @Override
    protected void onItemClick(User item) {
        UserProfileActivity.open(this, item.username);
    }

    @Override
    public void onResult(UserList users, Response<UserList> response) {
        hideLoading();

        setListAdapter(new UserAdapter(this, users));
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
