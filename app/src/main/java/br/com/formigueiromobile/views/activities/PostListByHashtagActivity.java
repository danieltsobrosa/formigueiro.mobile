package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.ListPostsByHashtagService;
import br.com.formigueiromobile.services.ListUsersService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroListActivity;
import br.com.formigueiromobile.views.viewAdapters.PostAdapter;
import br.com.formigueiromobile.views.viewAdapters.UserAdapter;
import retrofit.Response;

public class PostListByHashtagActivity extends FormigueiroListActivity<Post> implements ServiceClient<PostList> {

    public static void open(Context context, String hashtag) {
        Intent it = new Intent(context, PostListByHashtagActivity.class);
        it.putExtra("hashtag", hashtag);
        context.startActivity(it);
    }

    public PostListByHashtagActivity() {
        super(R.id.list);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_list_by_hashtag_activity);

        showLoading(R.string.loading, R.string.wait);

        Uri uri = getIntent().getData();
        String hashtag = null;

        try {
            hashtag = uri.toString().split("/")[3];
        } catch (Exception e) {}

        if (hashtag == null) {
            hashtag = getIntent().getStringExtra("hashtag");
        } else {
            hashtag = hashtag.substring(1);
        }

        Service service = new ListPostsByHashtagService(hashtag);
        executeService(service);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.posts_by_hashtag, menu);
        return true;
    }

    @Override
    protected void onItemClick(Post item) {
        PostDetailsActivity.open(this, item.id);
    }

    @Override
    public void onResult(PostList posts, Response<PostList> response) {
        hideLoading();

        setListAdapter(new PostAdapter(this, posts));
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
