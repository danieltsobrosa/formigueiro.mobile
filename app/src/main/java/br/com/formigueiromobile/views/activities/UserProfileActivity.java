package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.FollowService;
import br.com.formigueiromobile.services.ListUserPostsService;
import br.com.formigueiromobile.services.ListWhoIAmFollowService;
import br.com.formigueiromobile.services.RetrieveUserProfileService;
import br.com.formigueiromobile.services.UnfollowService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import br.com.formigueiromobile.views.activities.core.FormigueiroListActivity;
import br.com.formigueiromobile.views.utils.ImageUtils;
import br.com.formigueiromobile.views.viewAdapters.PostAdapter;
import retrofit.Response;

public class UserProfileActivity extends FormigueiroListActivity<Post> implements ServiceClient<User> {
    private ImageView photo;
    private TextView username;
    private TextView email;
    private Button follow;
    private Button unfollow;

    private String pUsername;

    private User user;

    public static void open(Context context, String username) {
        Intent it = new Intent(context, UserProfileActivity.class);
        it.putExtra("username", username);
        context.startActivity(it);
    }

    public UserProfileActivity() {
        super(R.id.postList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);

        photo = (ImageView) findViewById(R.id.photo);
        username = (TextView) findViewById(R.id.username);
        email = (TextView) findViewById(R.id.email);
        follow = (Button) findViewById(R.id.follow);
        unfollow = (Button) findViewById(R.id.unfollow);

        Uri uri = getIntent().getData();
        pUsername = null;

        try {
            pUsername = uri.toString().split("/")[3];
        } catch (Exception e) {}

        if (pUsername == null) {
            pUsername = getIntent().getStringExtra("username");
        } else {
            pUsername = pUsername.substring(1);
        }

        showLoading();
        final Context context = this;

        // Busca o perfil do usuário
        Service service = new RetrieveUserProfileService(pUsername);
        executeService(service);

        // Busca os posts do usuário
        Service postsService = new ListUserPostsService(pUsername);
        executeService(postsService, new ServiceClient<PostList>() {
            @Override
            public void onResult(PostList posts, Response<PostList> response) {
                setListAdapter(new PostAdapter(context, posts));
            }

            @Override
            public void onError(Exception e) {
                showMessage(e.getMessage());
            }

            @Override
            public void onSessionError(AuthenticationException e) {
                showMessage(e.getMessage());
            }
        });

        if (FormigueiroApplication.loggedUser == null) {
            return;
        }

        if (pUsername.equals(FormigueiroApplication.loggedUser.username)) {
            return;
        }

        // Verifica se estou seguindo ele
        Service followingService = new ListWhoIAmFollowService();
        executeService(followingService, new ServiceClient<UserList>() {
            @Override
            public void onResult(UserList list, Response response) {
                for (User user : list) {
                    if (user.username.equals(pUsername)) {
                        follow.setVisibility(View.INVISIBLE);
                        unfollow.setVisibility(View.VISIBLE);
                        return;
                    }
                }

                follow.setVisibility(View.VISIBLE);
                unfollow.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(Exception e) {
                showMessage(e.getMessage());
            }

            @Override
            public void onSessionError(AuthenticationException e) {
                showMessage(e.getMessage());
            }
        });
    }

    @Override
    public void onResult(User user, Response<User> response) {
        hideLoading();

        if (!response.isSuccess()) {
            showMessage(R.string.opsThereIsSomethingWrong);
            return;
        }

        this.user = user;
        ImageUtils.displayImage(this, user.photo.url, photo);
        username.setText(user.username);
        email.setText(user.email);
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    protected void onItemClick(Post post) {
        PostDetailsActivity.open(this, post.id);
    }

    public void doFollow(View view) {
        follow.setEnabled(false);
        Service followService = new FollowService(pUsername);
        executeService(followService, new ServiceClient() {
            @Override
            public void onResult(Object item, Response response) {
                follow.setEnabled(true);
                follow.setVisibility(View.INVISIBLE);
                unfollow.setVisibility(View.VISIBLE);
                FormigueiroApplication.following.add(user);
            }

            @Override
            public void onError(Exception e) {
                showMessage(e.getMessage());
                follow.setEnabled(true);
            }

            @Override
            public void onSessionError(AuthenticationException e) {
                showMessage(e.getMessage());
                follow.setEnabled(true);
            }
        });
    }

    public void doUnfollow(View view) {
        unfollow.setEnabled(false);
        Service unfollowService = new UnfollowService(pUsername);
        executeService(unfollowService, new ServiceClient() {
            @Override
            public void onResult(Object item, Response response) {
                unfollow.setEnabled(true);
                follow.setVisibility(View.VISIBLE);
                unfollow.setVisibility(View.INVISIBLE);

                executeService(new ListWhoIAmFollowService(true), new ServiceClient<UserList>() {
                    @Override
                    public void onResult(UserList users, Response response) {}

                    @Override
                    public void onError(Exception e) {}

                    @Override
                    public void onSessionError(AuthenticationException e) {}
                });
            }

            @Override
            public void onError(Exception e) {
                showMessage(e.getMessage());
                unfollow.setEnabled(true);
            }

            @Override
            public void onSessionError(AuthenticationException e) {
                showMessage(e.getMessage());
                unfollow.setEnabled(true);
            }
        });
    }
}
