package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.CreateUserService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

public class CreateUserActivity extends FormigueiroActivity implements ServiceClient<User> {
    private EditText username;
    private EditText email;
    private EditText password;

    public static void open(Context context, String username, String password) {
        Intent it = new Intent(context, CreateUserActivity.class);
        it.putExtra("username", username);
        it.putExtra("password", password);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_user_activity);

        username = (EditText) findViewById(R.id.username);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        Intent it = getIntent();
        username.setText(it.getStringExtra("username"));
        password.setText(it.getStringExtra("password"));
    }

    public void okClick(View view) {
        showLoading(R.string.saving, R.string.wait);

        User user = new User();
        user.username = username.getText().toString();
        user.email = email.getText().toString();
        user.password = password.getText().toString();

        Service service = new CreateUserService(user);
        executeService(service);
    }

    @Override
    public void onResult(User user, Response<User> response) {
        hideLoading();

        if (response.isSuccess()) {
            showMessage(R.string.userCreated);
            finish();
        } else {
            showMessage(R.string.opsThereIsSomethingWrong);
        }
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
