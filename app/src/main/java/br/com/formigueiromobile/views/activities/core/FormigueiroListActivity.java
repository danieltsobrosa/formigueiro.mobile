package br.com.formigueiromobile.views.activities.core;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by DANIEL on 04/10/2015.
 */
public abstract class FormigueiroListActivity<I> extends FormigueiroActivity implements AdapterView.OnItemClickListener {
        private ArrayAdapter<I> listAdapter;
    private final int listId;
    private ListView list;

    public FormigueiroListActivity(int listId) {
        super();

        this.listId = listId;
    }

    @Override
    public void setContentView(int layoutId) {
        super.setContentView(layoutId);

        list = (ListView) findViewById(listId);
        list.setOnItemClickListener(this);
    }

    protected void setListAdapter(ArrayAdapter<I> listAdapter) {
        this.listAdapter = listAdapter;
        list.setAdapter(listAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        I item = (I) adapterView.getItemAtPosition(i);
        onItemClick(item);
    }

    protected abstract void onItemClick(I item);
}
