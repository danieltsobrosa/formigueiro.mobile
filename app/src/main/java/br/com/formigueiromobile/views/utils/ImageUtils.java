package br.com.formigueiromobile.views.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.impl.FileCountLimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by DANIEL on 22/10/2015.
 */
public class ImageUtils {
    private static ImageLoader loader;

    private static void init(Context context) {
        if (loader == null) {
            File cacheDir = StorageUtils.getOwnCacheDirectory(
                    context.getApplicationContext(),
                    "/sdcard/Android/data/formigueiro_cache");

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true).cacheOnDisc(true).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    context.getApplicationContext()).defaultDisplayImageOptions(options)
                    .discCache(new FileCountLimitedDiscCache(cacheDir, 100))
                    .build();

            loader = ImageLoader.getInstance();
            loader.init(config);
        }
    }

    public static void displayImage(Context context, String src, ImageView view) {
        init(context);
        loader.displayImage(src, view);
    }

    public static void displayImage(Context context, Uri src, ImageView view) {
        init(context);
        view.setImageURI(src);
    }

    public static void displayImage(Context context, Bitmap src, ImageView view) {
        init(context);
        view.setImageBitmap(src);
    }

    public static String toBase64(Bitmap src) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        src.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
    }

}
