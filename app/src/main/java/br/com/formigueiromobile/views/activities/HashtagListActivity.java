package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Hashtag;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.HashtagList;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.services.ListHashtagsService;
import br.com.formigueiromobile.services.ListPostsService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroListActivity;
import br.com.formigueiromobile.views.viewAdapters.HashtagAdapter;
import br.com.formigueiromobile.views.viewAdapters.PostAdapter;
import retrofit.Response;

public class HashtagListActivity extends FormigueiroListActivity<Hashtag> implements ServiceClient<HashtagList> {

    public HashtagListActivity() {
        super(R.id.list);
    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, HashtagListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hashtag_list_activity);

        showLoading(R.string.loading, R.string.wait);
    }

    private void listHashtags() {
        Service service = new ListHashtagsService();
        executeService(service);
    }

    @Override
    protected void onResume() {
        super.onResume();

        listHashtags();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.hashtags, menu);
        return true;
    }

    @Override
    protected void onItemClick(Hashtag item) {
        PostListByHashtagActivity.open(this, item.id);
    }

    @Override
    public void onResult(HashtagList hashtags, Response<HashtagList> response) {
        hideLoading();

        setListAdapter(new HashtagAdapter(this, hashtags));
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
