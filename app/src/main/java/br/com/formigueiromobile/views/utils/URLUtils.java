package br.com.formigueiromobile.views.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import br.com.formigueiromobile.model.Location;

/**
 * Created by DANIEL on 14/11/2015.
 */
public final class URLUtils {

    private URLUtils() {}

    public static void navigateToURL(Context context, String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    public static void openGeolocation(Context context, Location location) {
        String url = "https://www.google.com.br/maps/@" + location.latitude + "," + location.longitude + ",17z";
        navigateToURL(context, url);
    }
}
