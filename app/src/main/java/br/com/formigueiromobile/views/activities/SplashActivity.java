package br.com.formigueiromobile.views.activities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.services.GetSessionIdService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

public class SplashActivity extends FormigueiroActivity implements ServiceClient<String> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Service service = new GetSessionIdService();
        executeService(service);
    }

    @Override
    public void onResult(String item, Response<String> response) {
        if (item == null) {
            LoginActivity.open(this);
        } else {
            PostListActivity.open(this);
        }
    }

    @Override
    public void onError(Exception e) {
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        LoginActivity.open(this);
    }
}
