package br.com.formigueiromobile.views.viewAdapters.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.formigueiromobile.R;

/**
 * Created by DANIEL on 04/10/2015.
 */
public abstract class BasicAdapter<I> extends ArrayAdapter<I> {

    public BasicAdapter(Context context, List<I> items) {
        super(context, R.layout.image_detail_item, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.detail_item, null);
        }

        I item = getItem(position);

        if (item != null) {
            TextView title = (TextView) v.findViewById(R.id.title);

            title.setText(getTitle(item));
        }

        return v;
    }

    protected abstract String getTitle(I item);
}
