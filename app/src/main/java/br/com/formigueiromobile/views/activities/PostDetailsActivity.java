package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Hashtag;
import br.com.formigueiromobile.model.Location;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.ListPostsByHashtagService;
import br.com.formigueiromobile.services.RetrievePostService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import br.com.formigueiromobile.views.activities.core.FormigueiroDetailActivity;
import br.com.formigueiromobile.views.utils.ImageUtils;
import br.com.formigueiromobile.views.utils.URLUtils;
import retrofit.Response;

public class PostDetailsActivity extends FormigueiroDetailActivity<Post> implements ServiceClient<Post> {
    private Context context;

    private TextView username;
    private TextView text;
    private ImageView photo;
    private ImageView geolocation;
    private ImageView image;
//    private LinearLayout hashtags;
//    private LinearLayout mentions;

    private Post post;

    public static void open(Context context, long id) {
        Intent it = new Intent(context, PostDetailsActivity.class);
        it.putExtra("id", id);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_details_activity);
        context = this;

        showLoading();

        Long id = getIntent().getLongExtra("id", 0L);

        username = (TextView) findViewById(R.id.username);
        text = (TextView) findViewById(R.id.text);
        photo = (ImageView) findViewById(R.id.photo);
        image = (ImageView) findViewById(R.id.image);
        geolocation = (ImageView) findViewById(R.id.geolocation);
//        hashtags = (LinearLayout) findViewById(R.id.hashtags);
//        mentions = (LinearLayout) findViewById(R.id.mentions);

        Service service = new RetrievePostService(id);
        executeService(service);
    }

    public void viewProfile(View view) {
        if (post != null) {
            UserProfileActivity.open(this, post.author.username);
        }
    }

    public void openMap(View view) {
        if (post.location != null) {
            URLUtils.openGeolocation(this, post.location);
        }
    }

    @Override
    public void onResult(Post post, Response<Post> response) {
        hideLoading();
        this.post = post;

//        for (Hashtag hashtag : post.hashtags) {
//            Button ht = new Button(this);
//            ht.setText("#" + hashtag.id);
//            ht.setOnClickListener(new HashtagClickHandler(hashtag));
//            hashtags.addView(ht);
//        }
//
//        for (User mention : post.mentions) {
//            Button ht = new Button(this);
//            ht.setText("@" + mention.username);
//            ht.setOnClickListener(new MentionClickHandler(mention));
//            mentions.addView(ht);
//        }

        username.setText(post.author.username);
        text.setText(post.text);
//        markHashtag(getApplicationContext(), post, text);

        Pattern matcher = Pattern.compile("[#]+[A-Za-z0-9-_]+");
        String url =    "content://br.com.formigueiromobile.postlistbyhashtag/";
        Linkify.addLinks(text, matcher, url);

        Pattern matcher2 = Pattern.compile("[@]+[A-Za-z0-9-_]+");
        String url2 =    "content://br.com.formigueiromobile.openuserprofile/";
        Linkify.addLinks(text, matcher2, url2);

        ImageUtils.displayImage(this, post.author.photo.url, photo);
        
        if (post.location != null) {
            geolocation.setVisibility(View.VISIBLE);
        }

        if (post.photos.size() > 0) {
            ImageUtils.displayImage(this, post.photos.get(0).url, image);
        }
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    abstract class MyClickHandler<T> implements View.OnClickListener {
        protected T item;

        public MyClickHandler(T item) {
            this.item = item;
        }
    }

    class HashtagClickHandler extends MyClickHandler<Hashtag> {
        public HashtagClickHandler(Hashtag item) { super(item); }

        @Override
        public void onClick(View view) {
            PostListByHashtagActivity.open(context, item.id);
        }
    }

    class MentionClickHandler extends MyClickHandler<User> {
        public MentionClickHandler(User item) { super(item); }

        @Override
        public void onClick(View view) {
            UserProfileActivity.open(context, item.username);
        }
    }

//    class ClickableSpanBase extends ClickableSpan {
//        private String textClicked;
//
//        public String getTextCliked(){
//            return this.textClicked;
//        }
//
//        @Override
//        public void onClick(View widget) {
//            TextView tv = (TextView) widget;
//
//            Spanned s = (Spanned) tv.getText();
//            int start = s.getSpanStart(this);
//            int end = s.getSpanEnd(this);
//            this.textClicked = s.subSequence(start, end).toString().replace("#","").trim();
//        }
//    }
//
//    public void markHashtag(final Context context, Post post, TextView textView){
//        if (post.hashtags != null) {
//            final Context context2 = context;
//            Pattern pattern = Pattern.compile("(?:\\s|\\A|^)[##]+([A-Za-z0-9-_]+)");
//            final Matcher matcher = pattern.matcher(post.text);
//            SpannableString ss = new SpannableString(post.text);
//
//            while (matcher.find()) {
//                ss.setSpan(new ClickableSpanBase() {
//
//                    @Override
//                    public void onClick(View textView) {
//                        super.onClick(textView);
//
//                        PostListByHashtagActivity.open(context, super.getTextCliked());
//                    }
//
//                    @Override
//                    public void updateDrawState(TextPaint ds) {
//                        super.updateDrawState(ds);
//                        ds.setUnderlineText(true);
//                    }
//                }, matcher.start() + 1, matcher.end(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//            }
//            textView.setMovementMethod(LinkMovementMethod.getInstance());
//            textView.setHighlightColor(Color.TRANSPARENT);
//            textView.setText(ss);
//        } else {
//            textView.setText(post.text);
//        }
//    }
}
