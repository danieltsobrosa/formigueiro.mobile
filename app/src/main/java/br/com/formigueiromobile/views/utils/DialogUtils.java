package br.com.formigueiromobile.views.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by DANIEL on 24/09/2015.
 */
public class DialogUtils {
    private static Dialog dialog;

    private DialogUtils() {}

    public static void showProgressDialog(Context context, String title, String message) {
        dialog = ProgressDialog.show(context, title, message, true);
    }

    public static void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
