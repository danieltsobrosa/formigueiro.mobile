package br.com.formigueiromobile.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Image;
import br.com.formigueiromobile.model.Location;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.UploadableFile;
import br.com.formigueiromobile.services.CreatePostService;
import br.com.formigueiromobile.services.GetLocationService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import br.com.formigueiromobile.views.utils.ImageUtils;
import br.com.formigueiromobile.views.utils.LoggedUserUtils;
import retrofit.Response;

public class CreatePostActivity extends FormigueiroActivity implements ServiceClient<Post> {
    private EditText text;
    private ImageView photo;
    private CheckBox checkin;

    private Bitmap bitmap;

    public static void open(Context context) {
        Intent it = new Intent(context, CreatePostActivity.class);

        if (LoggedUserUtils.requireLogged(context, it)) return;

        context.startActivity(it);
    }

    public static void open(Context context, long baseId) {
        Intent it = new Intent(context, CreatePostActivity.class);
        it.putExtra("baseId", baseId);
        context.startActivity(it);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_post_activity);

        text = (EditText) findViewById(R.id.text);
        photo = (ImageView) findViewById(R.id.photo);
        checkin = (CheckBox) findViewById(R.id.checkin);
    }

    public void okClick(View view) {
        showLoading(R.string.saving, R.string.wait);

        if (checkin.isChecked()) {
            Service geoService = new GetLocationService();
            executeService(geoService, new ServiceClient<Location>() {

                @Override
                public void onResult(Location location, Response<Location> response) {
                    createPost(location);
                }

                @Override
                public void onError(Exception e) {
                    showMessage(R.string.opsThereIsSomethingWrong);
                    hideLoading();
                }

                @Override
                public void onSessionError(AuthenticationException e) {}
            });

        } else {
            createPost();
        }
    }

    private void createPost(Location location) {
        Post post = new Post();
        post.text = text.getText().toString();

        if (bitmap != null) {
            post.photo = new UploadableFile("image/png", ImageUtils.toBase64(bitmap));
        }

        if (location != null) {
            post.location = location;
        }

        if (post.text.isEmpty()) {
            showMessage(R.string.textIsEmpty);
            hideLoading();
            return;
        }

        if (post.text.length() > 150) {
            showMessage(R.string.textTooLong);
            hideLoading();
            return;
        }

        if (post.photo != null && post.photo.content.getBytes().length > 2 * 1000 * 1000) {
            showMessage(R.string.photoIsTooWeight);
            hideLoading();
            return;
        }

        Service service = new CreatePostService(post);
        executeService(service);
    }

    private void createPost() {
        createPost(null);
    }

    @Override
    public void onResult(Post post, Response<Post> response) {
        hideLoading();

        if (response.isSuccess()) {
            showMessage(R.string.postCreated);
            finish();
        } else {
            showMessage(R.string.opsThereIsSomethingWrong);
        }
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    public void chooseImage(View view) {
        SelectImageActivity.open(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent it) {
        if (resultCode != Activity.RESULT_OK) return;

        switch (requestCode) {
            case SelectImageActivity.RESULT_ID:
                bitmap = it.getParcelableExtra("photo");
                ImageUtils.displayImage(this, bitmap, photo);
                break;
        }
    }
}
