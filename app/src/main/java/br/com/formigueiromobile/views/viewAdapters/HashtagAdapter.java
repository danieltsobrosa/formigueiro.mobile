package br.com.formigueiromobile.views.viewAdapters;

import android.content.Context;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.model.Hashtag;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.HashtagList;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.views.utils.ResourceUtils;
import br.com.formigueiromobile.views.viewAdapters.core.DetailAdapter;
import br.com.formigueiromobile.views.viewAdapters.core.ImageDetailAdapter;

/**
 * Created by Elzio on 13/10/2015.
 */
public class HashtagAdapter extends DetailAdapter<Hashtag> {

    public HashtagAdapter(Context context, HashtagList items) {
        super(context, items);
    }

    @Override
    protected String getTitle(Hashtag item) {
        return item.id;
    }

    @Override
    protected String getDetail(Hashtag item) {
        return ResourceUtils.stringResource(getContext(), R.string.usagesCount) + item.postsCount;
    }
}
