package br.com.formigueiromobile.views.viewAdapters;

import android.content.Context;

import java.util.List;

import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.model.collection.TodoList;
import br.com.formigueiromobile.views.viewAdapters.core.DetailAdapter;
import br.com.formigueiromobile.views.viewAdapters.core.ImageDetailAdapter;

/**
 * Created by DANIEL on 04/10/2015.
 */
public class TodoAdapter extends DetailAdapter<Todo> {

    public TodoAdapter(Context context, TodoList items) {
        super(context, items);
    }

    @Override
    protected String getTitle(Todo item) {
        return item.title;
    }

    @Override
    protected String getDetail(Todo item) {
        return item.completed ? "Concluído" : "Aberto";
    }
}
