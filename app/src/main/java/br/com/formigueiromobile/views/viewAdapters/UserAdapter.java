package br.com.formigueiromobile.views.viewAdapters;

import android.content.Context;

import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.views.viewAdapters.core.ImageDetailAdapter;

/**
 * Created by Elzio on 13/10/2015.
 */
public class UserAdapter extends ImageDetailAdapter<User> {

    public UserAdapter(Context context, UserList items) {
        super(context, items);
    }

    @Override
    protected Object getImage(User item) {
        return item.photo.url;
    }

    @Override
    protected String getTitle(User item) {
        return item.username;
    }

    @Override
    protected String getDetail(User item) {
        return item.email;
    }
}
