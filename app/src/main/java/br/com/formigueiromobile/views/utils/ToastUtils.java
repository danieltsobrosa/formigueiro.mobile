package br.com.formigueiromobile.views.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by DANIEL on 24/09/2015.
 */
public class ToastUtils {

    private ToastUtils() {}

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
