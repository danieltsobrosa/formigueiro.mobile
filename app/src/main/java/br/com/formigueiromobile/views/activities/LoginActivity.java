package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Session;
import br.com.formigueiromobile.services.LoginService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

public class LoginActivity extends FormigueiroActivity implements ServiceClient<Session> {
    private static Intent redirectoTo;

    private EditText login;
    private EditText password;

    public static void open(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    public static void open(Context context, Intent redirectoTo) {
        LoginActivity.redirectoTo = redirectoTo;
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.createUser:
                CreateUserActivity.open(this,
                        login.getText().toString(),
                        password.getText().toString());
                break;

            case R.id.anonimous:
                PostListActivity.open(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void executeLogin(View view) {
        showLoading();

        Service service = new LoginService(
                login.getText().toString(),
                password.getText().toString());

        executeService(service);
    }

    @Override
    public void onResult(Session session, Response<Session> response) {
        hideLoading();

        if (session == null) {
            showMessage(R.string.invalidUsernameOrPassword);
            return;
        }

        FormigueiroApplication.loggedUser = session.user;

        if (redirectoTo != null) {
            startActivity(redirectoTo);
        } else {
            PostListActivity.open(this);
        }
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
