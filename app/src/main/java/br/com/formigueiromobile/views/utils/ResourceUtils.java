package br.com.formigueiromobile.views.utils;

import android.content.Context;

/**
 * Created by DANIEL on 25/09/2015.
 */
public class ResourceUtils {

    private ResourceUtils() {}

    public static String stringResource(Context context, int stringId) {
        return context.getResources().getString(stringId);
    }

    public static String buildString(Context context, Object ...tokens) {
        StringBuilder sb = new StringBuilder();

        for (Object token : tokens) {
            if (token instanceof Integer) {
                sb.append(stringResource(context, (Integer) token));
            } else {
                sb.append(token);
            }
        }

        return sb.toString();
    }
}
