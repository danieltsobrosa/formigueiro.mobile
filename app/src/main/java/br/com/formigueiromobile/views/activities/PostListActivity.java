package br.com.formigueiromobile.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.services.ListPostsService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroListActivity;
import br.com.formigueiromobile.views.viewAdapters.PostAdapter;
import retrofit.Response;

public class PostListActivity extends FormigueiroListActivity<Post> implements ServiceClient<PostList> {

    public PostListActivity() {
        super(R.id.list);
    }

    public static void open(Context context) {
        context.startActivity(new Intent(context, PostListActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_list_activity);

        showLoading(R.string.loading, R.string.wait);
    }

    private void listPosts() {
        Service service = new ListPostsService();
        executeService(service);
    }

    @Override
    protected void onResume() {
        super.onResume();

        listPosts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.posts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.createPost:
                CreatePostActivity.open(this);
                break;

            case R.id.findUser:
                UserListActivity.open(this);
                break;

            case R.id.findByHashtag:
                HashtagListActivity.open(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onItemClick(Post item) {
        PostDetailsActivity.open(this, item.id);
    }

    @Override
    public void onResult(PostList posts, Response<PostList> response) {
        hideLoading();

        setListAdapter(new PostAdapter(this, posts));
    }

    @Override
    public void onError(Exception e) {
        hideLoading();
        showMessage(e.getMessage());
    }

    @Override
    public void onSessionError(AuthenticationException e) {
        hideLoading();
        showMessage(e.getMessage());
    }
}
