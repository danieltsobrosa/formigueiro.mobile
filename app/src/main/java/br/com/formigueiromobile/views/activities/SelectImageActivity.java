package br.com.formigueiromobile.views.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Image;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.ImageList;
import br.com.formigueiromobile.services.CreatePostService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import br.com.formigueiromobile.views.utils.ImageFilterUtils;
import br.com.formigueiromobile.views.utils.ImageUtils;
import retrofit.Response;

public class SelectImageActivity extends FormigueiroActivity {
    public static final int RESULT_ID = 437012487;

    private boolean imageSelected = false;
    private Uri imgUri;

    private Bitmap originalBitmap;
    private Bitmap grayBitmap;
    private Bitmap blackBitmap;
    private Bitmap sepiaBitmap;

    private ImageView photo;
    private ImageView gray;
    private ImageView black;
    private ImageView sepia;

    public static void open(FormigueiroActivity context) {
        SelectImageActivity.open(context, null);
    }

    public static void open(FormigueiroActivity context, Image photo) {
        Intent it = new Intent(context, SelectImageActivity.class);

        if (photo != null) {
            it.putExtra("photo", photo.url);
        }

        context.startActivity(it, RESULT_ID);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_image_activity);

        photo = (ImageView) findViewById(R.id.photo);
        gray = (ImageView) findViewById(R.id.gray);
        black = (ImageView) findViewById(R.id.black);
        sepia = (ImageView) findViewById(R.id.sepia);

        String url = getIntent().getStringExtra("photo");

        // Se não estiver editando, já abre a câmera
        if (url == null) {
            takePicture();
        } else {
            ImageUtils.displayImage(this, url, photo);
        }
    }

    public void chooseImage(View view) {
        if (!imageSelected) {
            takePicture();

        } else {
            Bitmap bmp = ((BitmapDrawable)photo.getDrawable()).getBitmap();
            returnResult(bmp);
        }
    }

    @Override
    protected void takePictureResult(Bitmap bitmap, Uri imageUri) {
        imgUri = imageUri;
        originalBitmap = bitmap;
        grayBitmap = ImageFilterUtils.grayScale(bitmap);
        blackBitmap = ImageFilterUtils.contrast(bitmap, 70);
        sepiaBitmap = ImageFilterUtils.sepia(bitmap, 1, 50, 70, 50);

        ImageUtils.displayImage(this, bitmap, photo);
        ImageUtils.displayImage(this, grayBitmap, gray);
        ImageUtils.displayImage(this, blackBitmap, black);
        ImageUtils.displayImage(this, sepiaBitmap, sepia);

        imageSelected = true;
    }

    @Override
    protected void editPictureResult(Uri imageUri) {
        ImageUtils.displayImage(this, imageUri, photo);


        grayBitmap = ImageFilterUtils.grayScale(originalBitmap);
        blackBitmap = ImageFilterUtils.contrast(originalBitmap, 70);
        sepiaBitmap = ImageFilterUtils.sepia(originalBitmap, 1, 50, 70, 50);

        ImageUtils.displayImage(this, grayBitmap, gray);
        ImageUtils.displayImage(this, blackBitmap, black);
        ImageUtils.displayImage(this, sepiaBitmap, sepia);

        ((TextView)findViewById(R.id.original)).setText(R.string.edited);
    }

    public void grayClick(View view) {
        returnResult(grayBitmap);
    }

    public void blackClick(View view) {
        returnResult(blackBitmap);
    }

    public void sepiaClick(View view) {
        returnResult(sepiaBitmap);
    }

    private void returnResult(Bitmap result) {
        Intent it = new Intent();
        it.putExtra("photo", result);

        if (getParent() == null) {
            setResult(Activity.RESULT_OK, it);
        } else {
            getParent().setResult(Activity.RESULT_OK, it);
        }

        finish();
    }

    public void androidEdit(View view) {
        editPicture(imgUri);
    }
}
