package br.com.formigueiromobile.views.utils;

import android.content.Context;
import android.content.Intent;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.R;
import br.com.formigueiromobile.views.activities.LoginActivity;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;

/**
 * Created by DANIEL on 18/11/2015.
 */
public final class LoggedUserUtils {

    private LoggedUserUtils() {}

    public static boolean requireLogged(Context context, Intent redirectTo) {
        if (FormigueiroApplication.loggedUser == null) {
            ToastUtils.showToast(context, ResourceUtils.stringResource(context, R.string.youMustBeLogged));
            LoginActivity.open(context, redirectTo);
            return true;
        }

        return false;
    }
}
