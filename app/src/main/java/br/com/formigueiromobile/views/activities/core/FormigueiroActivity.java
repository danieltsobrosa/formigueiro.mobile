package br.com.formigueiromobile.views.activities.core;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;

import java.io.File;

import br.com.formigueiromobile.R;
import br.com.formigueiromobile.services.LogoutService;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.services.core.ServiceExecutor;
import br.com.formigueiromobile.views.activities.LoginActivity;
import br.com.formigueiromobile.views.utils.DialogUtils;
import br.com.formigueiromobile.views.utils.ResourceUtils;
import br.com.formigueiromobile.views.utils.ToastUtils;

/**
 * Created by DANIEL on 24/09/2015.
 */
public abstract class FormigueiroActivity extends Activity {
    private static final int TAKE_PICTURE = 1;
    private static final int EDIT_PICTURE = 2;

    public static final String FORMIGUEIRO = "br.com.formigueiromobile";

    private Bundle savedInstanceState;
    private Uri tmpImageUri;

    public FormigueiroActivity() {
        tmpImageUri = Uri.fromFile(new File("/sdcard/tmpImage"));
    }

    protected Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;

        // Dá a capacidade a esta tela de receber mensagens de evento
        //EventUtils.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Retira a habilidade de receber eventos parar a tela
        //EventUtils.unregister(this);
    }

    protected void showMessage(int message) {
        showMessage(stringResource(message));
    }

    protected void showMessage(String message) {
        ToastUtils.showToast(this, message);
    }

    protected void showLoading() {
        showLoading(R.string.loading, R.string.wait);
    }

    protected void showLoading(int title) {
        showLoading(stringResource(title));
    }

    protected void showLoading(String title) {
        showLoading(R.string.loading, title);
    }

    protected void showLoading(int title, int message) {
        showLoading(stringResource(title), stringResource(message));
    }

    protected void showLoading(String title, int message) {
        showLoading(title, stringResource(message));
    }

    protected void showLoading(int title, String message) {
        showLoading(stringResource(title), message);
    }

    protected void showLoading(String title, String message) {
        DialogUtils.showProgressDialog(this, title, message);
    }

    protected void hideLoading() {
        DialogUtils.hideDialog();
    }

    public void executeService(Service service, ServiceClient client) {
        service.setActivity(this);
        service.setClient(client);
        ServiceExecutor.executeService(service);
    }

    public void executeService(Service service) {
        service.setActivity(this);
        service.setClient((ServiceClient) this);
        ServiceExecutor.executeService(service);
    }

    protected String stringResource(int stringId) {
        return ResourceUtils.stringResource(this, stringId);
    }

    protected String buildString(Object ...tokens) {
        return ResourceUtils.buildString(this, tokens);
    }

    public void takePicture() {
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (it.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(it, TAKE_PICTURE);
        } else {
            showMessage(R.string.opsThereIsSomethingWrong);
        }
    }

    public void editPicture(Uri imgUri) {
        Intent it = new Intent(Intent.ACTION_EDIT);
        it.setDataAndType(imgUri, "image/*");
        it.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(Intent.createChooser(it, null), EDIT_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }

        if (resultCode != RESULT_OK) {
            showMessage(R.string.opsThereIsSomethingWrong);
            return;
        }

        Bundle extras = data.getExtras();

        switch (requestCode) {
            case TAKE_PICTURE:
                Bitmap image = (Bitmap) extras.get("data");

                String strUri = MediaStore.Images.Media.insertImage(getContentResolver(), image,
                        "The fucking crazy image", "");

                Uri uri = Uri.parse(strUri);

                takePictureResult(image, uri);
                break;

            case EDIT_PICTURE:
                editPictureResult(data.getData());
        }
    }

    protected void takePictureResult(Bitmap image, Uri imageUri) {}
    protected void editPictureResult(Uri imageUri) {}

    public void startActivity(Intent it, int resultId) {
        startActivityForResult(it, resultId);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.logout:
                executeService(new LogoutService());
                LoginActivity.open(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
