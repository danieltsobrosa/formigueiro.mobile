package br.com.formigueiromobile.views.viewAdapters;

import android.content.Context;

import java.util.List;

import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.views.viewAdapters.core.DetailAdapter;
import br.com.formigueiromobile.views.viewAdapters.core.ImageDetailAdapter;

/**
 * Created by Elzio on 13/10/2015.
 */
public class PostAdapter extends ImageDetailAdapter<Post> {

    public PostAdapter(Context context, PostList items) {
        super(context, items);
    }

    @Override
    protected Object getImage(Post item) {
        return item.author.photo.url;
    }

    @Override
    protected String getTitle(Post item) {
        return item.author.username;
    }

    @Override
    protected String getDetail(Post item) {
        return item.text;
    }

    @Override
    protected boolean hasImage(Post item) {
        return !item.photos.isEmpty();
    }

    @Override
    protected boolean hasGeolocation(Post item) {
        return item.location != null;
    }
}
