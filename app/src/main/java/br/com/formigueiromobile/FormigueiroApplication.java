package br.com.formigueiromobile;

import android.app.Application;

import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.GetSessionIdService;
import br.com.formigueiromobile.services.core.Service;

/**
 * Created by DANIEL on 30/10/2015.
 */
public class FormigueiroApplication extends Application {
    public static final String SESSION_ID = "br.com.fomigueiromobile.SESSION_ID";

    public static String sessionId = null;
    public static User loggedUser = null;
    public static UserList following = null;
    
}
