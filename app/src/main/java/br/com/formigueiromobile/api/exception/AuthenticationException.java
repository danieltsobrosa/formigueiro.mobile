package br.com.formigueiromobile.api.exception;

/**
 * Created by DANIEL on 19/10/2015.
 */
public class AuthenticationException extends Exception {

    public AuthenticationException (String message) {
        super(message);
    }
}
