package br.com.formigueiromobile.api.core;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import retrofit.Call;

/**
 * Created by DANIEL on 27/10/2015.
 */
public class APICallProxy implements InvocationHandler {
    private final Object impl;

    public APICallProxy(Object impl) {
        this.impl = impl;
    }

    @Override
    public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(impl, args);

        if (!(result instanceof Call)) return result;

        return Proxy.newProxyInstance(
                Call.class.getClassLoader(),
                new Class[]{Call.class},
                new InvocationProxy(result)
        );
    }
}
