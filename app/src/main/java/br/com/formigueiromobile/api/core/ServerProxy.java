package br.com.formigueiromobile.api.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Retrofit;
import retrofit.GsonConverterFactory;

/**
 * Created by DANIEL on 03/09/2015.
 */
public class ServerProxy {
    private static int TIMEOUT_TIME = 20000; // 20 segundos

    private static Retrofit placeholderServer;
    private static Retrofit papagaioServer;

    private ServerProxy() {}

    public static Retrofit getPlaceholderServer() {
        Gson gson = new GsonBuilder().create();

        if (placeholderServer == null) {
            placeholderServer = new Retrofit.Builder()
                    .baseUrl("http://jsonplaceholder.typicode.com/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return placeholderServer;
    }

    public static Retrofit getPapagaioServer() {
        if (papagaioServer == null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();

            papagaioServer = new Retrofit.Builder()
                    .baseUrl("https://papagaio.ml/api/")
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            OkHttpClient client = papagaioServer.client();

            client.setConnectTimeout(TIMEOUT_TIME, TimeUnit.SECONDS);
            client.setReadTimeout(TIMEOUT_TIME, TimeUnit.SECONDS);
            client.setWriteTimeout(TIMEOUT_TIME, TimeUnit.SECONDS);
            client.interceptors().add(new AuthenticationInterceptor());
//            client.interceptors().add(new LogInterceptor());
        }

        return papagaioServer;
    }
}
