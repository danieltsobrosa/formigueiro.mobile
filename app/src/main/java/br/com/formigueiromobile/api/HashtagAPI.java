package br.com.formigueiromobile.api;

import java.util.List;

import br.com.formigueiromobile.model.Hashtag;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.HashtagList;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by DANIEL on 22/10/2015.
 */
public interface HashtagAPI {

    @POST("hashtag")
    Call<Hashtag> create(@Body Hashtag hashtag);

    @GET("hashtag/{id}")
    Call<Hashtag> retrieve(@Path("id") String id);

    @DELETE("hashtag/{id}")
    void delete(@Path("id") String id);

    @GET("hashtag")
    Call<HashtagList> list();
}
