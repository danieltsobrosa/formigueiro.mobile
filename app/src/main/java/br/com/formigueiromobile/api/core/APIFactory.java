package br.com.formigueiromobile.api.core;

import java.lang.reflect.Proxy;

import br.com.formigueiromobile.api.HashtagAPI;
import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.SessionAPI;
import br.com.formigueiromobile.api.TodoAPI;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.UserPostsAPI;
import retrofit.Retrofit;

/**
 * Created by DANIEL on 22/09/2015.
 */
public class APIFactory {

    private APIFactory() {}

    public static TodoAPI getTodoAPI() {
        return create(ServerProxy.getPlaceholderServer(), TodoAPI.class);
    }

    public static SessionAPI getSessionAPI() {
        return create(ServerProxy.getPapagaioServer(), SessionAPI.class);
    }

    public static PostAPI getPostAPI() {
        return create(ServerProxy.getPapagaioServer(), PostAPI.class);
    }

    public static UserAPI getUserAPI() {
        return create(ServerProxy.getPapagaioServer(), UserAPI.class);
    }

    public static UserPostsAPI getUserPostsAPI() {
        return create(ServerProxy.getPapagaioServer(), UserPostsAPI.class);
    }

    public static HashtagAPI getHashtagAPI() {
        return create(ServerProxy.getPapagaioServer(), HashtagAPI.class);
    }

    private static <T> T create (Retrofit server, Class<T> cls) {
        return (T) Proxy.newProxyInstance(
                cls.getClassLoader(),
                new Class[]{cls},
                new APICallProxy(server.create(cls))
        );
    }
}
