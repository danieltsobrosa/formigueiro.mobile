package br.com.formigueiromobile.api.core;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import retrofit.Response;


import br.com.formigueiromobile.api.exception.AuthenticationException;

/**
 * Created by DANIEL on 19/10/2015.
 */
public class InvocationProxy implements InvocationHandler {
    private final static JsonParser jsonParser = new JsonParser();
    private final Object impl;

    public InvocationProxy (Object impl) {
        this.impl = impl;
    }

    @Override
    public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {
        try {
            Object rs = method.invoke(this.impl, args);

            if (!(rs instanceof Response)) return rs;

            Response res = (Response) rs;

            if (res.isSuccess()) return res;

            JsonObject object = jsonParser.parse(res.errorBody().string()).getAsJsonObject();
            String msg = String.format(
                    "[%s] %s",
                    object.get("err").getAsString(),
                    object.get("message").getAsString()
            );

            switch (res.code()) {
                case 401: throw new AuthenticationException(msg);
                default:
                    throw new Exception(msg);
            }

        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }
}
