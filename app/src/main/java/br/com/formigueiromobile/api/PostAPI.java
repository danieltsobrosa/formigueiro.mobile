package br.com.formigueiromobile.api;

import java.util.List;

import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.PostList;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by DANIEL on 14/10/2015.
 */
public interface PostAPI {

    @POST("post")
    Call<Post> create(@Body Post post);

    @GET("post/{id}")
    Call<Post> retrieve(@Path("id") Long id);

    @GET("post")
    Call<PostList> list();

//    @PUT("post/{id}/like")
//    Call<Post> like(@Path("id") Long id);
//
//    @DELETE("post/{id}/like")
//    Call<Post> dislike(@Path("id") Long id);

    @GET("hashtag/{hashtag}/post")
    Call<PostList> listByHashtag(@Path("hashtag") String hashtag);
}
