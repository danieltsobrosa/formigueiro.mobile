package br.com.formigueiromobile.api;

import java.util.List;

import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.UserList;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by DANIEL on 22/10/2015.
 */
public interface UserFollowerAPI {

    @GET("user/{username}/follower")
    Call<UserList> list(@Path("username") String username);

    // --

    @GET("user/me/follower")
    Call<UserList> listOfLoggedUser();
}
