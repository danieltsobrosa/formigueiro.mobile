package br.com.formigueiromobile.api.core;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by DANIEL on 21/10/2015.
 */
public class LogInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        Log.d("foo", String.format("%s %s - %d", request.method(), request.url(), response.code()));
        Log.d("foo", response.body().string());
        return response;
    }
}
