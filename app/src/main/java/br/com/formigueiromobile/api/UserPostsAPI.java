package br.com.formigueiromobile.api;

import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.PostList;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by DANIEL on 14/10/2015.
 */
public interface UserPostsAPI {

    @GET("user/{username}/post")
    Call<PostList> list(@Path("username") String username);
}
