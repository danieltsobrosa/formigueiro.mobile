package br.com.formigueiromobile.api.core;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import br.com.formigueiromobile.FormigueiroApplication;

/**
 * Created by DANIEL on 19/10/2015.
 */
public class AuthenticationInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (FormigueiroApplication.sessionId != null) {
            request = request.newBuilder()
                    .addHeader("Authorization", "Bearer " + FormigueiroApplication.sessionId)
                    .build();
        }

        return chain.proceed(request);
    }
}
