package br.com.formigueiromobile.api;

import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.Credentials;
import br.com.formigueiromobile.model.Session;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by DANIEL on 13/10/2015.
 */
public interface SessionAPI {

    @POST("session")
    Call<Session> create(@Body Credentials credentials) throws AuthenticationException;

    @GET("session/{id}")
    Call<Session> retrieve(@Path("id") String id);

    @DELETE("session/{id}")
    void delete(@Path("id") String id);
}
