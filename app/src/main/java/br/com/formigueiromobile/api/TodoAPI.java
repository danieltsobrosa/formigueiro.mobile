package br.com.formigueiromobile.api;

import java.util.List;

import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.model.collection.TodoList;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by DANIEL on 03/09/2015.
 */
public interface TodoAPI {

    @GET("/todos/{id}")
    Call<Todo> retrieve(@Path("id") Integer id);

    @GET("/todos")
    Call<TodoList> list();
}
