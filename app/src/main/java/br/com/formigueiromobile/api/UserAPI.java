package br.com.formigueiromobile.api;

import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.model.collection.UserList;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by DANIEL on 22/10/2015.
 */
public interface UserAPI {

    @POST("user")
    Call<User> create(@Body User user);

    @GET("user/{username}")
    Call<User> retrieve(@Path("username") String username);

    @PUT("user/{username}")
    Call<User> update(@Path("username") String username, @Body User user);

    @GET("user")
    Call<UserList> list(@Query("search") String search);

    // --

    @GET("user/me")
    Call<User> retrieveLoggedUser();

    @GET("user/me/following")
    Call<UserList> listFollowing();

    @FormUrlEncoded
    @POST("user/me/following")
    Call<Object> follow(@Field("user") String username) throws Exception;

    @DELETE("user/me/following/{username}")
    Call<Object> unfollow(@Path("username") String username);
}
