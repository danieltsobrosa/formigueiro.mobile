package br.com.formigueiromobile.services;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 29/10/2015.
 */
public class GetLoggedUserService extends Service<User> {
    @Override
    public void execute() throws Exception {
        if (FormigueiroApplication.loggedUser == null) {
            UserAPI api = APIFactory.getUserAPI();
            Response<User> res = api.retrieveLoggedUser().execute();

            FormigueiroApplication.loggedUser = res.body();
            result(FormigueiroApplication.loggedUser, res);

        } else {
            result(FormigueiroApplication.loggedUser, null);
        }
    }
}
