package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class CreatePostService extends Service<Post> {
    private Post post;

    public CreatePostService(Post post) {
        this.post = post;
    }

    @Override
    public void execute() throws Exception{
        PostAPI api = APIFactory.getPostAPI();

        Response<Post> res = api.create(this.post).execute();
        result(res.body(), res);
    }
}
