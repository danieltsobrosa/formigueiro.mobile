package br.com.formigueiromobile.services;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by Daniel T. Sobrosa on 14/11/2015.
 */
public class FollowService extends Service {
    private String username;

    public FollowService(String username) {
        this.username = username;
    }

    @Override
    public void execute() throws Exception {
        UserAPI api = APIFactory.getUserAPI();
        Response res = api.follow(username).execute();
        result(res.body(), res);
    }
}
