package br.com.formigueiromobile.services;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;

/**
 * Created by DANIEL on 29/10/2015.
 */
public class LogoutService extends Service<Object> {

    @Override
    public void execute() throws Exception {
        FormigueiroApplication.sessionId = null;
        FormigueiroApplication.loggedUser = null;
        FormigueiroApplication.following = new UserList();

        Context context = getActivity().getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences(FormigueiroActivity.FORMIGUEIRO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FormigueiroApplication.SESSION_ID, null);
        editor.commit();
    }
}
