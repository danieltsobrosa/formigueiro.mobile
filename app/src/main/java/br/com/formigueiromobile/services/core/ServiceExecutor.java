package br.com.formigueiromobile.services.core;

import android.content.Context;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by DANIEL on 02/09/2015.
 */
public class ServiceExecutor {
    private static ServiceExecutor instance;
    private ExecutorService pool;

    private ServiceExecutor() {
        pool = Executors.newFixedThreadPool(3);
    }

    public static ServiceExecutor getInstance() {
        if (instance == null) {
            instance = new ServiceExecutor();
        }
        return instance;
    }

    public static void executeService(Service service) {
        try {
            getInstance().execute(service);
            //service.errorHandlingExecute();
        } catch (Exception e) {
            e.printStackTrace();
            service.error(e);
        }
    }

    public void execute(Service service) throws Exception {
        Callable wrapper = new ServiceWrapper(service);
        Future future = pool.submit(wrapper);
    }

    private class ServiceWrapper implements Callable {
        private Service service;

        ServiceWrapper(Service service) {
            this.service = service;
        }

        @Override
        public Object call() throws Exception {
            service.errorHandlingExecute();
            return null;
        }
    }
}
