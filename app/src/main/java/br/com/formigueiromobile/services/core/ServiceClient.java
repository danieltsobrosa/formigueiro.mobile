package br.com.formigueiromobile.services.core;

import br.com.formigueiromobile.api.exception.AuthenticationException;
import retrofit.Response;

/**
 * Created by DANIEL on 26/10/2015.
 */
public interface ServiceClient<T> {
    public void onResult(T item, Response<T> response);
    public void onError(Exception e);
    public void onSessionError(AuthenticationException e);
}
