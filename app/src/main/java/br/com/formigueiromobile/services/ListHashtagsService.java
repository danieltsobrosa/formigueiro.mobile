package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.HashtagAPI;
import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Hashtag;
import br.com.formigueiromobile.model.collection.HashtagList;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by ElzioJr on 13/10/2015.
 */
public class ListHashtagsService extends Service<HashtagList> {

    @Override
    public void execute() throws Exception {
        HashtagAPI api = APIFactory.getHashtagAPI();

        Response<HashtagList> res = api.list().execute();
        result(res.body(), res);
    }
}
