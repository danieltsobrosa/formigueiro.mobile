package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.TodoAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 02/09/2015.
 */
public class RetrieveTodoService extends Service<Todo> {
    private final int todoId;

    public RetrieveTodoService(int todoId) {
        this.todoId = todoId;
    }

    @Override
    public void execute() throws Exception {
        // Obtém um ponto de entrada do servidor
        TodoAPI api = APIFactory.getTodoAPI();

        Response<Todo> res = api.retrieve(todoId).execute();
        result(res.body(), res);
    }
}
