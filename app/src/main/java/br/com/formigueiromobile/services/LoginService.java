package br.com.formigueiromobile.services;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.api.SessionAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Credentials;
import br.com.formigueiromobile.model.Session;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class LoginService extends Service<Session> {
    private Credentials credentials;

    public LoginService(String username, String password) {
        this.credentials = new Credentials(username, password);
    }

    @Override
    public void execute() throws Exception{
        SessionAPI api = APIFactory.getSessionAPI();

        Response<Session> res = api.create(this.credentials).execute();

        if (res.isSuccess()) {
            Session session = res.body();
            FormigueiroApplication.sessionId = session.id;
            //FormigueiroApplication.loggedUser = session.user;

            Context context = getActivity().getApplicationContext();
            SharedPreferences sharedPref = context.getSharedPreferences(FormigueiroActivity.FORMIGUEIRO, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(FormigueiroApplication.SESSION_ID, FormigueiroApplication.sessionId);
            editor.commit();
        }

        result(res.body(), res);
    }
}
