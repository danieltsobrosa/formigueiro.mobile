package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by ElzioJr on 13/10/2015.
 */
public class ListPostsByHashtagService extends Service<PostList> {
    private String search;

    public ListPostsByHashtagService(String search) {
        this.search = search;
    }

    @Override
    public void execute() throws Exception {
        PostAPI api = APIFactory.getPostAPI();

        Response<PostList> res = api.listByHashtag(search).execute();
        result(res.body(), res);
    }
}
