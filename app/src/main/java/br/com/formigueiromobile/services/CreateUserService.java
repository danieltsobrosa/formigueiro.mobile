package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.SessionAPI;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Credentials;
import br.com.formigueiromobile.model.Session;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 13/10/2015.
 */
public class CreateUserService extends Service<User> {
    private User user;

    public CreateUserService(User user) {
        this.user = user;
    }

    @Override
    public void execute() throws Exception{
        UserAPI api = APIFactory.getUserAPI();

        Response<User> res = api.create(this.user).execute();
        result(res.body(), res);
    }
}
