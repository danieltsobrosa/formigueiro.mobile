package br.com.formigueiromobile.services;

import java.util.List;

import br.com.formigueiromobile.api.TodoAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.model.collection.TodoList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 02/09/2015.
 */
public class ListTodoService extends Service<TodoList> {

    @Override
    public void execute() throws Exception {
        TodoAPI api = APIFactory.getTodoAPI();

        Response<TodoList> res = api.list().execute();
        result(res.body(), res);
    }
}
