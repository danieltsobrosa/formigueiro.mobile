package br.com.formigueiromobile.services;

import java.util.List;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by ElzioJr on 13/10/2015.
 */
public class ListPostsService extends Service<PostList> {

    @Override
    public void execute() throws Exception {
        PostAPI api = APIFactory.getPostAPI();

        Response<PostList> res = api.list().execute();
        result(res.body(), res);
    }
}
