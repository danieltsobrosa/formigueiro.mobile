package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 27/10/2015.
 */
public class RetrieveUserProfileService extends Service<User> {
    private final String username;

    public RetrieveUserProfileService(String username) {
        this.username = username;
    }

    @Override
    public void execute() throws Exception {
        // Obtém um ponto de entrada do servidor
        UserAPI api = APIFactory.getUserAPI();

        Response<User> res = api.retrieve(username).execute();
        result(res.body(), res);
    }
}
