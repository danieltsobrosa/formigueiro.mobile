package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.UserPostsAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by ElzioJr on 13/10/2015.
 */
public class ListUserPostsService extends Service<PostList> {
    private String username;

    public ListUserPostsService(String username) {
        this.username = username;
    }

    @Override
    public void execute() throws Exception {
        UserPostsAPI api = APIFactory.getUserPostsAPI();

        Response<PostList> res = api.list(username).execute();
        result(res.body(), res);
    }
}
