package br.com.formigueiromobile.services;


import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;

import java.util.List;

import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import retrofit.Response;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.*;
import com.google.android.gms.location.LocationServices;

/**
 * Created by DANIEL on 09/11/2015.
 */
public class GetLocationService extends Service<br.com.formigueiromobile.model.Location>
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;

    @Override
    public void execute() throws Exception {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            br.com.formigueiromobile.model.Location loc = new br.com.formigueiromobile.model.Location(
                    mLastLocation.getLatitude(),
                    mLastLocation.getLongitude());

            result(loc, null);
        } else {
            result(new br.com.formigueiromobile.model.Location(-29.7190397, -51.4826922), null);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(GetLocationService.class.getSimpleName(), "Connection Suspended: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        error(new Exception("Não foi possível pegar a localização: " + connectionResult.getErrorCode()));
    }
}
