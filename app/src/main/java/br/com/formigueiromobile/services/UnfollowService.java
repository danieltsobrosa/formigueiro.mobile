package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by Daniel T. Sobrosa on 14/11/2015.
 */
public class UnfollowService extends Service {
    private String username;

    public UnfollowService(String username) {
        this.username = username;
    }

    @Override
    public void execute() throws Exception {
        UserAPI api = APIFactory.getUserAPI();
        Response res = api.unfollow(username).execute();
        result(res.body(), res);
    }
}
