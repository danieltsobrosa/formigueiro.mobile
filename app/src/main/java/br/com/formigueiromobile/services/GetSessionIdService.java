package br.com.formigueiromobile.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.model.User;
import br.com.formigueiromobile.services.core.Service;
import br.com.formigueiromobile.services.core.ServiceClient;
import br.com.formigueiromobile.views.activities.LoginActivity;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

/**
 * Created by DANIEL on 29/10/2015.
 */
public class GetSessionIdService extends Service<String> {

    @Override
    public void execute() throws Exception {
        if (FormigueiroApplication.sessionId == null) {
            Context context = getActivity().getApplicationContext();
            SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(FormigueiroActivity.FORMIGUEIRO, Context.MODE_PRIVATE);
            FormigueiroApplication.sessionId = sharedPref.getString(FormigueiroApplication.SESSION_ID, null);
        }

        if (FormigueiroApplication.sessionId != null && FormigueiroApplication.loggedUser == null) {
            Service srv = new GetLoggedUserService();
            getActivity().executeService(srv, new ServiceClient<User>() {
                @Override
                public void onResult(User loggedUser, Response<User> response) {
                    result(FormigueiroApplication.sessionId, null);
                }

                @Override
                public void onError(Exception e) {
                    throw new RuntimeException(e);
                }

                @Override
                public void onSessionError(AuthenticationException e) {
                    throw new RuntimeException(e);
                }
            });
        } else {
            result(FormigueiroApplication.sessionId, null);
        }
    }
}
