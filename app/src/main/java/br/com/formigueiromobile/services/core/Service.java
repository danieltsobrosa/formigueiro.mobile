package br.com.formigueiromobile.services.core;

import android.app.Activity;
import br.com.formigueiromobile.api.exception.AuthenticationException;
import br.com.formigueiromobile.views.activities.core.FormigueiroActivity;
import retrofit.Response;

/**
 * Created by DANIEL on 02/09/2015.
 */
public abstract class Service<T> {
    private FormigueiroActivity activity;
    private ServiceClient client;

    public FormigueiroActivity getActivity() {
        return activity;
    }

    public void setActivity(FormigueiroActivity activity) {
        this.activity = activity;
    }

    public ServiceClient getClient() {
        return client;
    }

    public void setClient(ServiceClient client) {
        this.client = client;
    }

    void errorHandlingExecute() {
        try {
            execute();

        } catch (Exception e) {
            if (e.getCause() instanceof AuthenticationException) {
                sessionError((AuthenticationException) e.getCause());
            } else {
                e.printStackTrace();
                error(e);
            }
        }
    }

    public abstract void execute() throws Exception;

    public void error(Exception err) {
        activity.runOnUiThread(new UIThreadRunnableError(err));
    }

    public void result(T item, Response<T> res) {
        activity.runOnUiThread(new UIThreadRunnableResult(item, res));
    }

    public void sessionError(AuthenticationException authErr) {
        activity.runOnUiThread(new UIThreadRunnableSessionError(authErr));
    }

    class UIThreadRunnableResult implements Runnable {
        private T item;
        private Response<T> res;

        public UIThreadRunnableResult(T item, Response<T> res) {
            this.item = item;
            this.res = res;
        }

        @Override
        public void run() {
            ServiceClient<T> client = (ServiceClient<T>) getClient();
            client.onResult(item, res);
        }
    }

    class UIThreadRunnableError implements Runnable {
        private Exception err;

        public UIThreadRunnableError(Exception err) {
            this.err = err;
        }

        @Override
        public void run() {
            ServiceClient<T> client = (ServiceClient<T>) getClient();
            client.onError(err);
        }
    }

    class UIThreadRunnableSessionError implements Runnable {
        private AuthenticationException authErr;

        public UIThreadRunnableSessionError(AuthenticationException authErr) {
            this.authErr = authErr;
        }
        public void run() {
            ServiceClient<T> client = (ServiceClient<T>) getClient();
            client.onSessionError(authErr);
        }
    }

}
