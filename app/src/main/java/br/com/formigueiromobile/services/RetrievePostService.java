package br.com.formigueiromobile.services;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.TodoAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.Post;
import br.com.formigueiromobile.model.Todo;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by DANIEL on 02/09/2015.
 */
public class RetrievePostService extends Service<Post> {
    private final long postId;

    public RetrievePostService(long postId) {
        this.postId = postId;
    }

    @Override
    public void execute() throws Exception {
        // Obtém um ponto de entrada do servidor
        PostAPI api = APIFactory.getPostAPI();

        Response<Post> res = api.retrieve(postId).execute();
        result(res.body(), res);
    }
}
