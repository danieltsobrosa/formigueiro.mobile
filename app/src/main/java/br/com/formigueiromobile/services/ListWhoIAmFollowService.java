package br.com.formigueiromobile.services;

import br.com.formigueiromobile.FormigueiroApplication;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by Daniel T. Sobrosa on 14/11/2015.
 */
public class ListWhoIAmFollowService extends Service<UserList> {
    private boolean force;

    public ListWhoIAmFollowService() {
        this(false);
    }

    public ListWhoIAmFollowService(boolean force) {
        this.force = force;
    }

    @Override
    public void execute() throws Exception {
        UserList users = FormigueiroApplication.following;
        Response<UserList> res = null;

        if (users == null || force) {
            UserAPI api = APIFactory.getUserAPI();
            res = api.listFollowing().execute();
            FormigueiroApplication.following = users = res.body();
        }

        result(users, res);
    }
}
