package br.com.formigueiromobile.services;

import java.util.List;

import br.com.formigueiromobile.api.PostAPI;
import br.com.formigueiromobile.api.UserAPI;
import br.com.formigueiromobile.api.core.APIFactory;
import br.com.formigueiromobile.model.collection.PostList;
import br.com.formigueiromobile.model.collection.UserList;
import br.com.formigueiromobile.services.core.Service;
import retrofit.Response;

/**
 * Created by ElzioJr on 13/10/2015.
 */
public class ListUsersService extends Service<UserList> {
    private String search;

    public ListUsersService(String search) {
        this.search = search;
    }

    @Override
    public void execute() throws Exception {
        UserAPI api = APIFactory.getUserAPI();

        Response<UserList> res = api.list(search).execute();
        result(res.body(), res);
    }
}
